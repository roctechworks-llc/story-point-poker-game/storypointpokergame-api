import { randomUUID } from "node:crypto";
import { createServer } from "node:http";
import { Server } from "socket.io";

const server = createServer();

const PORT = 3000;

const MINUTE = 1000 * 60;
const MAX_SESSION_LENGTH = 5;

const io = new Server(server, {
  cors: {
    origin: ["https://storypointpokergame.com","https://www.storypointpokergame.com"],
  },
  connectionStateRecovery: {
    maxDisconnectionDuration: 5 * MINUTE,
    skipMiddlewares: true,
  },
});

/*
  There's a one in (2 x letters.length) ** MAX_SESSION_LENGTH chance of repetition.
  How many people will honestly use this?
  Increase MAX_SESSION_LENGTH if things get dodgy.
  Could probably add lowercase as well.
*/
const generateSessionId = () => {
  const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const alphabet = `${letters}${letters.toLowerCase()}`;
  return Array.from({length: MAX_SESSION_LENGTH}, () => alphabet[Math.floor(Math.random() * alphabet.length - 1)]).join("");
};

/*
  Used for templating new sessions.
*/
const BLANK_SESSION = {
  moderatorId: null,
  moderatorNickname: null,
  deckTypeId: null,
  votes: {},
};

/*
  Holds active sessions and their relevant information.
  Surely this is a good enough substitute for needing a database.
*/
const activeSessions = {};

const getSessionData = (sessionId) => {
  if (sessionId in activeSessions) {
    const sessionData = activeSessions[sessionId];
    return {
      ...sessionData,
      castedVotes: Object.values(activeSessions[sessionId].votes)
        .filter((v) => v !== null).length,
      votableGuests: Object.keys(activeSessions[sessionId].votes).length,
    };
  }
  return {};
};

const generateSession = () => {
  const id = generateSessionId();

  activeSessions[id] = {
    ...BLANK_SESSION,
    votes: {
      ...BLANK_SESSION.votes,
    },
  };
  return [id, getSessionData(id)];
};

const generateSessionMetrics = (id) => {
  const session = getSessionData(id);
  // Filter out strings for math functions below
  const values = Object.values(session.votes);

  const valueCountMap = {};

  values.forEach((v) => {
    if (!(v in valueCountMap)) {
      valueCountMap[v] = 0;
    }

    valueCountMap[v]++;
  });

  // Might be a tie.
  const mode = [];
  let highest = Math.max(...Object.values(valueCountMap));


  for (const [k, v] of Object.entries(valueCountMap)) {
    if ( v === highest) {
      mode.push(k);
    }
  }

  return {
    min: Math.min(...values.filter((v) => typeof v === "number")) || 0,
    max: Math.max(...values.filter((v) => typeof v === "number")) || 0,
    mode,
    itemized: values,
  }
};

const resetVotes = (id) => {
  const session = activeSessions[id];
  Object.keys(session.votes).forEach((k) => {
    session.votes[k] = null;
  });
};

io.on("connection", (socket) => {
  socket.on("disconnect", () => {
    console.log('disconnect')
  });

  socket.on("session:create", (data) => {
    const {
      username,
      deckTypeId,
    } = data;

    const [sessionId, session] = generateSession();

    socket.join(sessionId);

    const userId = randomUUID();

    session.moderatorId = userId;
    session.moderatorNickname = username;
    session.deckTypeId = deckTypeId;

    activeSessions[sessionId] = session
    
    /*
      A moderator can't vote.
      Isn't it usually a product owner who runs these things?
    */

    //session.votes[userId] = null;

    socket.emit("session:created", {
      id: sessionId,
      userId,
    });
  });

  socket.on("session:join", (data) => {
    let {
      user,
      session,
    } = data;

    // Can probably send this to some middleware
    if (!(session in activeSessions)) {
      return socket.emit("session:none");
    }

    socket.join(session);

    const { moderatorId } = getSessionData(session);

    if (!user) {
      user = randomUUID();
    }

    if (user !== moderatorId) {
      activeSessions[session].votes[user] = null;
    } 

    io.to(session).emit("session:guest-change", getSessionData(session));

    socket.emit("session:joined", {
      id: session,
      userId: user,
    });
  });

  socket.on("session:vote", (data) => {
    const {
      user,
      session,
      vote,
    } = data;

    if (!(session in activeSessions)) {
      socket.emit("session:none");
      return;
    }

    activeSessions[session].votes[user] = vote;

    const anonymizedData = getSessionData(session);
    delete anonymizedData.votes;

    io.to(session).emit("session:voted", anonymizedData);
  });

  socket.on("session:reveal", (data) => {
    const {
      user,
      session,
    } = data;

    if (!(session in activeSessions)) {
      return socket.emit("session:none");
    }

    const { moderatorId } = getSessionData(session);

    // Users can't reveal votes.
    if (user !== moderatorId) {
      return;
    }

    io.to(session).emit("session:revealed", generateSessionMetrics(session));
    resetVotes(session);
  });

  socket.on("session:leave", (data) => {
    const {
      user,
      session,
    } = data;
    
    if (!(session in activeSessions)) {
      return socket.emit("session:none");
    }

    const usersSession = getSessionData(session);

    delete usersSession.votes[user];

    io.to(session).emit("session:guest-change", getSessionData(session));
    socket.emit("session:left");
  });
});

server.listen(PORT, () => {
  console.log("Running on: ", PORT);
});